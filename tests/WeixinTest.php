<?php

namespace weixinunit;


use Yii;

class weixinTest extends TestCase
{
    /**
     * @var \zonday\weixin\Weixin
     */
    protected $weixin;

    public function setUp()
    {
        $this->mockWebApplication();
        $this->weixin = Yii::$app->weixin;
    }

    public function testCheckSignature()
    {
        $time = time();
        $nonce = Yii::$app->getSecurity()->generateRandomString(6);
        $arr = array($time, $nonce, $this->weixin->token);
        sort($arr, SORT_STRING);
        $query = [
            'signature' => sha1(implode($arr)),
            'nonce' => $nonce,
            'timestamp' => $time,
        ];
        $this->assertTrue($this->weixin->checkSignature($query));
    }

    public function testJsapiSignature()
    {
        $timestamp = '1438228236';
        $nonceStr = 'VD1OxI';
        $ticket = 'sM4AOVdWfPE4DxkXGEs8VFBRxHXW4zJRr-S9sQJpmXOCyCP_L-j7C5PMFUJmiEQGcDzlVOxZAGCnNgVKWTJUsQ';
        $url = 'http://52.68.254.177/yii2-weixin-test/app/web/index.php?r=jsapi%2Findex';
        $result = $this->weixin->jsApiConfig($url, [], [
            'timestamp' => $timestamp,
            'nonceStr' => $nonceStr,
            'ticket' => $ticket,
        ]);
        $signature = sha1("jsapi_ticket=$ticket&noncestr=$nonceStr&timestamp=$timestamp&url=$url");
        $this->assertEquals($signature, $result['signature']);
    }
}