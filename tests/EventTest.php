<?php

namespace weixinunit;


use Yii;

class EventTest extends TestCase
{
    /**
     * @var \zonday\weixin\Weixin
     */
    protected $weixin;

    public function setUp()
    {
        $this->mockWebApplication();
        $this->weixin = Yii::$app->weixin;
    }

    public function testSubscribe()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[FromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[subscribe]]></Event>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\event\Subscribe', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $event = $message->event;
        $this->assertEquals('subscribe', $event);
    }

    public function testUnsubscribe()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[FromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[unsubscribe]]></Event>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\event\Unsubscribe', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $event = $message->event;
        $this->assertEquals('unsubscribe', $event);
    }

    public function testScan()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[FromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[SCAN]]></Event>
<EventKey><![CDATA[SCENE_VALUE]]></EventKey>
<Ticket><![CDATA[TICKET]]></Ticket>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\event\Scan', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('TICKET', 'Ticket', $message);
        $event = $message->event;
        $this->assertEquals('SCAN', $event);
    }

    public function testLocation()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[fromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[LOCATION]]></Event>
<Latitude>23.137466</Latitude>
<Longitude>113.352425</Longitude>
<Precision>119.385040</Precision>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\event\Location', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('23.137466', 'Latitude', $message);
        $event = $message->event;
        $this->assertEquals('LOCATION', $event);
    }

    public function testClick()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[FromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[CLICK]]></Event>
<EventKey><![CDATA[EVENTKEY]]></EventKey>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\event\Click', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('EVENTKEY', 'EventKey', $message);
        $event = $message->event;
        $this->assertEquals('CLICK', $event);
    }

    public function testView()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[FromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[VIEW]]></Event>
<EventKey><![CDATA[www.qq.com]]></EventKey>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\event\View', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('www.qq.com', 'EventKey', $message);
        $event = $message->event;
        $this->assertEquals('VIEW', $event);
    }

    protected function generateSignParams()
    {
        $time = time();
        $nonce = Yii::$app->getSecurity()->generateRandomString(6);
        $arr = array($time, $nonce, $this->weixin->token);
        sort($arr, SORT_STRING);
        $query = [
            'signature' => sha1(implode($arr)),
            'nonce' => $nonce,
            'timestamp' => $time,
        ];
        return $query;
    }
}