<?php

namespace weixinunit;


use DOMDocument;
use Yii;
use zonday\weixin\crypt\WXBizMsgCrypt;

class WXBizMsgCryptTest extends TestCase
{
    /**
     * @var \zonday\weixin\Weixin
     */
    protected $weixin;

    public function setUp()
    {
        $this->mockWebApplication();
        $this->weixin = Yii::$app->weixin;
        $this->weixin->encodingAesKey = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFG';
    }

    public function testWXBizMsgCrypt()
    {
        $text = "<xml><ToUserName><![CDATA[oia2Tj我是中文jewbmiOUlr6X-1crbLOvLw]]></ToUserName><FromUserName><![CDATA[gh_7f083739789a]]></FromUserName><CreateTime>1407743423</CreateTime><MsgType><![CDATA[video]]></MsgType><Video><MediaId><![CDATA[eYJ1MbwPRJtOvIEabaxHs7TX2D-HV71s79GUxqdUkjm6Gs2Ed1KF3ulAOA9H1xG0]]></MediaId><Title><![CDATA[testCallBackReplyVideo]]></Title><Description><![CDATA[testCallBackReplyVideo]]></Description></Video></xml>";
        $timeStamp = "1409304348";
        $nonce = "xxxxxx";

        $pc = new WXBizMsgCrypt($this->weixin->token, $this->weixin->encodingAesKey, $this->weixin->appId);
        $encryptMsg = '';
        $errCode = $pc->encryptMsg($text, $timeStamp, $nonce, $encryptMsg);
        $this->assertEquals($errCode, 0);

        $xml_tree = new DOMDocument();
        $xml_tree->loadXML($encryptMsg);
        $array_e = $xml_tree->getElementsByTagName('Encrypt');
        $array_s = $xml_tree->getElementsByTagName('MsgSignature');
        $encrypt = $array_e->item(0)->nodeValue;
        $msg_sign = $array_s->item(0)->nodeValue;

        $format = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        $from_xml = sprintf($format, $encrypt);

        $msg = '';
        $errCode = $pc->decryptMsg($msg_sign, $timeStamp, $nonce, $from_xml, $msg);
        $this->assertEquals($errCode, 0);
    }
}