<?php

namespace weixinunit;


use Yii;

class MessageTest extends TestCase
{
    /**
     * @var \zonday\weixin\Weixin
     */
    protected $weixin;

    public function setUp()
    {
        $this->mockWebApplication();
        $this->weixin = Yii::$app->weixin;
    }

    public function testText()
    {
        $data = <<<EOT
<xml>
 <ToUserName><![CDATA[toUser]]></ToUserName>
 <FromUserName><![CDATA[fromUser]]></FromUserName>
 <CreateTime>1348831860</CreateTime>
 <MsgType><![CDATA[text]]></MsgType>
 <Content><![CDATA[this is a test]]></Content>
 <MsgId>1234567890123456</MsgId>
 </xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\message\Text', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('this is a test', 'Content', $message);
        $msgType = $message->msgType;
        $this->assertEquals('text', $msgType);
    }

    public function textImage()
    {
        $data = <<<EOT
 <xml>
 <ToUserName><![CDATA[toUser]]></ToUserName>
 <FromUserName><![CDATA[fromUser]]></FromUserName>
 <CreateTime>1348831860</CreateTime>
 <MsgType><![CDATA[image]]></MsgType>
 <PicUrl><![CDATA[this is a url]]></PicUrl>
 <MediaId><![CDATA[media_id]]></MediaId>
 <MsgId>1234567890123456</MsgId>
 </xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\message\Image', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('this is a a url', 'PicUrl', $message);
        $msgType = $message->msgType;
        $this->assertEquals('image', $msgType);
    }

    public function textVoice()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[fromUser]]></FromUserName>
<CreateTime>1357290913</CreateTime>
<MsgType><![CDATA[voice]]></MsgType>
<MediaId><![CDATA[media_id]]></MediaId>
<Format><![CDATA[Format]]></Format>
<MsgId>1234567890123456</MsgId>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\message\Voice', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $msgType = $message->msgType;
        $this->assertEquals('voice', $msgType);
    }

    public function testVideo()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[fromUser]]></FromUserName>
<CreateTime>1357290913</CreateTime>
<MsgType><![CDATA[video]]></MsgType>
<MediaId><![CDATA[media_id]]></MediaId>
<ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
<MsgId>1234567890123456</MsgId>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\message\Video', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $msgType = $message->msgType;
        $this->assertEquals('video', $msgType);
    }

    public function testShortVideo()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[fromUser]]></FromUserName>
<CreateTime>1357290913</CreateTime>
<MsgType><![CDATA[shortvideo]]></MsgType>
<MediaId><![CDATA[media_id]]></MediaId>
<ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
<MsgId>1234567890123456</MsgId>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\message\ShortVideo', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $msgType = $message->msgType;
        $this->assertEquals('shortvideo', $msgType);
    }

    public function testLocation()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[fromUser]]></FromUserName>
<CreateTime>1351776360</CreateTime>
<MsgType><![CDATA[location]]></MsgType>
<Location_X>23.134521</Location_X>
<Location_Y>113.358803</Location_Y>
<Scale>20</Scale>
<Label><![CDATA[位置信息]]></Label>
<MsgId>1234567890123456</MsgId>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\message\Location', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('20', 'Scale', $message);
        $msgType = $message->msgType;
        $this->assertEquals('location', $msgType);
    }

    public function testLink()
    {
        $data = <<<EOT
<xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[fromUser]]></FromUserName>
<CreateTime>1351776360</CreateTime>
<MsgType><![CDATA[link]]></MsgType>
<Title><![CDATA[公众平台官网链接]]></Title>
<Description><![CDATA[公众平台官网链接]]></Description>
<Url><![CDATA[url]]></Url>
<MsgId>1234567890123456</MsgId>
</xml>
EOT;
        $signParams = $this->generateSignParams();
        $message = $this->weixin->receive($data, $signParams);
        $this->assertInstanceOf('\zonday\weixin\message\Link', $message);
        $this->assertAttributeSame('toUser', 'ToUserName', $message);
        $this->assertAttributeSame('url', 'Url', $message);
        $msgType = $message->msgType;
        $this->assertEquals('link', $msgType);
    }

    protected function generateSignParams()
    {
        $time = time();
        $nonce = Yii::$app->getSecurity()->generateRandomString(6);
        $arr = array($time, $nonce, $this->weixin->token);
        sort($arr, SORT_STRING);
        $query = [
            'signature' => sha1(implode($arr)),
            'nonce' => $nonce,
            'timestamp' => $time,
        ];
        return $query;
    }
}