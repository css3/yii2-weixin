#yii2-weixin
##安装
```
  "require": {
    "zonday/yii2-weixin": "dev-master"
  },
  "repositories": [
    {
      "type": "vcs",
      "url": "git@git.oschina.net:css3/yii2-weixin.git"
    }
  ],
```
##配置
```
    'components' => [
        'weixin' => [
            'class' => '\zonday\weixin\Weixin',
            'appId' => '',
            'appSecret' => '',
            'token' => '',
        ],
    ],
```
##使用
```
Yii::$app->weixin
```

Api

```
use zonday\weixin\api;

$userApi = new api\User;
$userApi->info($openid);
```

demo: https://git.oschina.net/css3/yii2-weixin-test