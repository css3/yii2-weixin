<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\api;

use yii\helpers\FileHelper;
use zonday\weixin\Weixin;

class Material extends Api
{
    /**
     * 新增永久图文素材
     * @see http://mp.weixin.qq.com/wiki/14/7e6c03263063f4813141c3e17dd4350a.html
     * @param array $post
     * @return mixed
     */
    public function addNews(array $post)
    {
        return $this->request('material/add_news', null, $post);
    }

    /**
     * 更新永久图文素材
     * @see http://mp.weixin.qq.com/wiki/14/7e6c03263063f4813141c3e17dd4350a.html
     * @param array $post
     * @return mixed
     */
    public function updateNews(array $post)
    {
        return $this->request('material/update_news', null, $post);
    }

    /**
     * 新增永久其他素材 Curl方式
     * @see http://mp.weixin.qq.com/wiki/14/7e6c03263063f4813141c3e17dd4350a.html
     * @param string $type 素材类型
     * @param string $mediaPath 文件路径
     * @param array $description 视频的描述信息
     * @param string $mediaName 文件名
     * @return mixed
     */
    public function addMaterial($type, $mediaPath, $description = [], $mediaName = '')
    {
        $mediaName = $mediaName ? $mediaName : basename($mediaPath);
        if (function_exists('curl_file_create')) {
            $media = curl_file_create($mediaPath, FileHelper::getMimeType($mediaPath), $mediaName);
        } else {
            $media = '@' . $mediaPath;
        }

        $post = ['type' => $type, 'media' => $media];
        if ($type === 'video') {
            if ( !isset($description['title'], $description['introduction'])) {
                throw new \InvalidArgumentException('类型为video的素材，必须有description参数');
            } else {
                $post['description'] = json_encode($description, JSON_UNESCAPED_UNICODE);
            }
        }

        return $this->request('material/add_material', null, $post, ['jsonEncode' => false]);
    }

    /**
     * 获取永久素材
     * @see http://mp.weixin.qq.com/wiki/4/b3546879f07623cb30df9ca0e420a5d0.html
     * @param $mediaId
     * @return mixed
     */
    public function getMaterial($mediaId)
    {
        return $this->request('material/get_material', null, ['mediaId' => $mediaId]);
    }

    /**
     * 删除永久素材
     * @see http://mp.weixin.qq.com/wiki/5/e66f61c303db51a6c0f90f46b15af5f5.html
     * @param string $mediaId
     * @return mixed
     */
    public function delMaterial($mediaId)
    {
        return $this->request('material/del_material', null, ['mediaId' => $mediaId]);
    }

    /**
     * 获取永久素材数量
     * @see http://mp.weixin.qq.com/wiki/16/8cc64f8c189674b421bee3ed403993b8.html
     * @return mixed
     */
    public function getMaterialCount()
    {
        return $this->request('material/get_materialcount');
    }

    /**
     * 分类获取永久素材列表
     * @see http://mp.weixin.qq.com/wiki/12/2108cd7aafff7f388f41f37efa710204.html
     * @param string $type 类型
     * @param integer $offset 偏移量
     * @param integer $count 数量
     * @return mixed
     */
    public function batchGetMaterial($type, $offset, $count)
    {
        return $this->request('material/batchget_material', null, ['type' => $type, 'offset' => $offset, 'count' => $count]);
    }

    /**
     * 获取上传表单Url
     * @return string
     */
    public function getFormUrl()
    {
        return Weixin::API_BASE_URL . '/material/add_material?access_token=' . $this->getWeixin()->getAccessToken();
    }
}
