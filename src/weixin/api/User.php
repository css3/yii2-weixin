<?php
/**
 * @author css3 <css3@qq.com>
 */
namespace zonday\weixin\api;

/**
 * Class User
 */
class User extends Api
{
    /**
     * 用户基本信息
     * @see http://mp.weixin.qq.com/wiki/14/bb5031008f1494a59c6f71fa0f319c66.html
     * @param string $openid 用户的openid
     * @return mixed
     */
    public function info($openid)
    {
        return $this->request('user/info', ['openid' => $openid]);
    }

    /**
     * 批量获取用户信息
     * @see http://mp.weixin.qq.com/wiki/14/bb5031008f1494a59c6f71fa0f319c66.html
     * @param array $userList [['openid': '', 'lang': 'zh-CN'], ... ]
     * @return mixed
     */
    public function infoBatchGet(array $userList)
    {
        return $this->request('user/info/batchget', null, ['user_list' => $userList]);
    }

    /**
     * 更新用户备注名
     * @see http://mp.weixin.qq.com/wiki/1/4a566d20d67def0b3c1afc55121d2419.html
     * @param string $openid 用户openid
     * @param string $remark 用户备注
     * @return mixed
     */
    public function updateRemark($openid, $remark)
    {
        return $this->request('user/info/updateremark', null, ['openid' => $openid, 'remark' => $remark]);
    }

    /**
     * 获取用户列表
     * @see http://mp.weixin.qq.com/wiki/0/d0e07720fc711c02a3eab6ec33054804.html
     * @param string $nextOpenid
     * @return mixed
     */
    public function get($nextOpenid = '')
    {
        if ($nextOpenid !== '') {
            $get['next_openid'] = $nextOpenid;
        } else {
            $get = null;
        }
        return $this->request('user/get', $get);
    }
}
