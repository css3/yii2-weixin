<?php
/**
 * @author css3 <css3@qq.com>
 */
namespace zonday\weixin\api;

/**
 * Class Groups
 */
class Groups extends Api
{
    /**
     * 创建分组
     * @see http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html
     * @param string $name
     * @return mixed
     */
    public function create($name)
    {
        return $this->request('groups/create', null, ['group' => ['name' => $name]]);
    }

    /**
     * 查询所有分组
     * @see http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html
     * @return mixed
     */
    public function get()
    {
        return $this->request('groups/get');
    }

    /**
     * 查询用户所在分组
     * @see http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html
     * @param $openid
     * @return mixed
     */
    public function getId($openid)
    {
        return $this->request('groups/getid', null, ['openid' => $openid]);
    }

    /**
     * 修改分组名
     * @see http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html
     * @param string $id 组id
     * @param string $name 组名
     * @return mixed
     */
    public function update($id, $name)
    {
        return $this->request('groups/update', null, ['group' => ['id' => $id, 'name' => $name]]);
    }

    /**
     * 更新用户分组
     * @see http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html
     * @param string $openid 用户openid
     * @param string $toGroupId 移动到的组id
     * @return mixed
     */
    public function membersUpdate($openid, $toGroupId)
    {
        return $this->request('groups/members/update', null, ['openid' => $openid, 'to_groupid' => $toGroupId]);
    }

    /**
     * 批量移动用户分组
     * @see http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html
     * @param array $openids
     * @param string $toGroupId
     * @return mixed
     */
    public function membersBatchUpdate(array $openids, $toGroupId)
    {
        return $this->request('groups/members/batchupdate', null, ['openid_list' => $openids, 'to_groupid' => $toGroupId]);
    }

    /**
     * 删除分组
     * @see http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html
     * @param string $id 分组id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->request('groups/delete', null, ['group' => ['id' => $id]]);
    }
}
