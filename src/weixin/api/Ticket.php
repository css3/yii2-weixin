<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\api;

/**
 * Class Ticket
 */
class Ticket extends Api
{
    /**
     * 获取ticket
     * @see http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
     * @param string $type
     * @return mixed
     */
    public function getTicket($type)
    {
        return $this->request('ticket/getticket', ['type' => $type]);
    }
}
