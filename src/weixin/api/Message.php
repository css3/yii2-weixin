<?php
/**
 * @author css3 <css3@qq.com>
 */
namespace zonday\weixin\api;
use zonday\weixin\exception\WeixinException;

/**
 * Class Message
 */
class Message extends Api
{
    /**
     * 根据分组进行群发
     * @see  http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html
     * @param array $post
     * @return mixed
     */
    public function massSendAll(array $post)
    {
        return $this->request('message/mass/sendall', null, $post);
    }

    /**
     * 根据OpenID列表群发
     * @see http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html
     * @param array $post
     * @return mixed
     */
    public function massSend(array $post)
    {
        return $this->request('message/mass/send', null, $post);
    }

    /**
     * 删除群发
     * @see http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html
     * @param $msgId
     * @return mixed
     */
    public function massDelete($msgId)
    {
        return $this->request('message/mass/delete', null, ['msg_id' => $msgId]);
    }

    /**
     * 预览接口
     * @see http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html
     * @param array $post
     * @return mixed
     */
    public function massPreview(array $post)
    {
        return $this->request('message/mass/preview', null, $post);
    }

    /**
     * 查询群发状态
     * @see http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html
     * @param $msgId
     * @return mixed
     */
    public function massGet($msgId)
    {
        return $this->request('message/mass/get', null, ['msg_id' => $msgId]);
    }

    /**
     * 模板消息发送
     * @see http://mp.weixin.qq.com/wiki/17/304c1885ea66dbedf7dc170d84999a9d.html
     * @param array $post
     * @return mixed
     */
    public function templateSend(array $post)
    {
        return $this->request('message/template/send', null, $post);
    }

    protected $message;

    /**
     * 生成客服文本信息
     *
     * @param string $openid 用户的openid
     * @param $text
     * @return Message $this
     */
    public function textMsg($openid, $text)
    {
        $text = addslashes($text);
        $this->message = <<<EOT
"touser": "{$openid}",
"msgtype": "text",
"text": {
    "content": "{$text}"
}
EOT;
        return $this;
    }

    /**
     * 生成客服图片消息
     *
     * @param string $openid 用户的openid
     * @param $mediaId
     * @return Message $this
     */
    public function imageMsg($openid, $mediaId)
    {
        $this->message = <<<EOT
"touser": "{$openid}",
"msgtype": "image",
"image": {
    "media_id": "{$mediaId}"
}
EOT;
        return $this;
    }

    /**
     * 生成客服语音信息
     *
     * @param string $openid 用户的openid
     * @param $mediaId
     * @return Message $this
     */
    public function voiceMsg($openid, $mediaId)
    {
        $this->message = <<<EOT
"touser": "{$openid}",
"msgtype": "voice",
"voice": {
    "media_id": "{$mediaId}"
}
EOT;
        return $this;
    }

    /**
     * 生成客服视频信息
     *
     * @param string $openid 用户的openid
     * @param $mediaId
     * @param string $title
     * @param string $description
     * @return Message $this
     */
    public function videoMsg($openid, $mediaId, $title = '', $description = '')
    {
        $title = addslashes($title);
        $description = addslashes($description);
        $this->message = <<<EOT
"touser": "{$openid}",
"msgtype": "video",
"video": {
    "media_id": "{$mediaId}",
    "title": "{$title}",
    "description" "{$description}"
}
EOT;
        return $this;
    }

    /**
     * 生成客服音乐信息
     *
     * @param string $openid 用户的openid
     * @param string $title
     * @param string $description
     * @param string $musicUrl
     * @param string $hqMusicUrl
     * @param $thumbMediaId
     * @return Message $this
     */
    public function musicMsg($openid, $thumbMediaId, $musicUrl, $hqMusicUrl,  $title = '', $description = '' )
    {
        $this->message = <<<EOT
"touser": "{$openid}",
"msgtype": "video",
"music": {
    "title": "{$title}",
    "description" "{$description}",
    "musicurl": "{$musicUrl}",
    "hqmusicurl": "{$hqMusicUrl}",
    "thumb_media_id": "{$thumbMediaId}"
}
EOT;
        return $this->message;
    }

    /**
     * @param $openid
     * @param $cardId
     * @param $cardExt
     * @return Message $this
     */
    public function cardMsg($openid, $cardId, $cardExt)
    {
        $this->message = <<<EOT
"touser":"{$openid}",
"msgtype":"wxcard",
"wxcard":{
    "card_id":"{$cardId}",
    "card_ext": "{$cardExt}"
}
EOT;
        return $this;
    }

    /**
     * 生成客服图文信息
     *
     * @param string $openid 用户的openid
     * @param $articles
     * @return Message $this
     */
    public function newsMsg($openid, $articles)
    {
        $output = array();
        foreach ($articles as $article) {
            $str = array();
            if (isset($article['title'])) {
                $str[] = '"title":"' . addslashes($article['title']) . '"';
            }
            if (isset($article['description'])) {
                $str[] = '"description":"' . addslashes($article['description']) . '"';
            }
            if (isset($article['url'])) {
                $str[] = '"url":"' . $article['url'] . '"';
            }
            if (isset($article['picUrl'])) {
                $str[] = '"picurl":"' . $article['picUrl'] . '"';
            }
            $output[] = '{' . implode(',', $str) . '}';
        }
        $output = implode(',', $output);
        $this->message = <<<EOT
"touser":"{$openid}",
"msgtype":"news",
"news":{
    "articles": [
        {$output}
    ]
}
EOT;
        return $this;
    }

    /**
     * 发送消息
     * @param string $kfAccount 客服账号
     * @return mixed
     * @throws \zonday\weixin\exception\WeixinException
     */
    public function customSend($kfAccount = '')
    {
        if (!$this->message) {
            throw new WeixinException('没有消息被发送');
        }
        $message = "{\n" . $this->message;
        if ($kfAccount) {
            $message .= ",\n" . '"customservice": {"kf_account": "' . $kfAccount. '"}"';
        }
        $message .= "\n}";
        $result = $this->request('message/custom/send', null, $message);
        $this->message = '';
        return $result;
    }
}
