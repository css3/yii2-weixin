<?php
/**
 * @author css3 <css3@qq.com>
 */
namespace zonday\weixin\api;

class CustomService extends Api
{
    /**
     * 添加客服帐号
     * ```
     * $post = [
     *    "kf_account" : "test1@test",
     *    "nickname" : "客服1",
     *    "password" : "pswmd5",
     * ]
     * ```
     * @see http://mp.weixin.qq.com/wiki/1/70a29afed17f56d537c833f89be979c9.html
     * @param array $post
     * @return mixed
     */
    public function kfAccountAdd($post = [])
    {
        return $this->request('customservice/kfaccount/add', null, $post);
    }

    /**
     * 修改客服帐号
     * ```
     * $post = [
     *    "kf_account" : "test1@test",
     *    "nickname" : "客服1",
     *    "password" : "pswmd5",
     * ]
     * ```
     * @see http://mp.weixin.qq.com/wiki/1/70a29afed17f56d537c833f89be979c9.html
     * @param array $post
     * @return mixed
     */
    public function kfAccountUpdate($post = [])
    {
        return $this->request('customservice/kfaccount/update', null, $post);
    }

    /**
     * 删除客服帐号
     * ```
     * $post = [
     *    "kf_account" : "test1@test",
     *    "nickname" : "客服1",
     *    "password" : "pswmd5",
     * ]
     * ```
     * @see http://mp.weixin.qq.com/wiki/1/70a29afed17f56d537c833f89be979c9.html
     * @param array $post
     * @return mixed
     */
    public function kfAccountDel($post = [])
    {
        return $this->request('customservice/kfaccount/del', null, $post);
    }

    /**
     * 上传客服头像
     * @see http://mp.weixin.qq.com/wiki/1/70a29afed17f56d537c833f89be979c9.html
     * @param string $kfAccount 客服账号
     * @param array $post
     * @return mixed
     */
    public function uploadHeadimg($kfAccount, $post = [])
    {
        return $this->request('customservice/kfaccount/uploadheading', ['kf_account' => $kfAccount], $post);
    }

    /**
     * 获取所有客服账号
     * @see http://mp.weixin.qq.com/wiki/1/70a29afed17f56d537c833f89be979c9.html
     * @return mixed
     */
    public function getKfList()
    {
        return $this->request('customservice/getkflist');
    }

    /**
     * 获取在线客服接待信息
     * @see http://mp.weixin.qq.com/wiki/9/6fff6f191ef92c126b043ada035cc935.html
     * @return mixed
     */
    public function getOnlineKfList()
    {
        return $this->request('customservice/getkonlinekflist');
    }
}
