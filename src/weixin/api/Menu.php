<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\api;

/**
 * Class Menu
 */
class Menu extends Api
{
    /**
     * 创建餐单
     * @see http://mp.weixin.qq.com/wiki/13/43de8269be54a0a6f64413e4dfa94f39.html
     * @param $post
     * @return mixed
     */
    public function create($post)
    {
        return $this->request('menu/create', null, $post);
    }

    /**
     * 获取餐单
     * @see http://mp.weixin.qq.com/wiki/16/ff9b7b85220e1396ffa16794a9d95adc.html
     * @return mixed
     */
    public function get()
    {
        return $this->request('menu/get');
    }

    /**
     * 删除餐单
     * @see https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN
     * @return mixed
     */
    public function delete()
    {
        return $this->request('menu/delete');
    }
}
