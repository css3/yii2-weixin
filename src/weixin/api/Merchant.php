<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\api;

/**
 * Class Merchant
 */
class Merchant extends Api
{
    const API_BASE_URL = 'https://api.weixin.qq.com/merchant';

    /**
     * 创建商品
     * @param array $post
     * @return mixed
     */
    public function create(array $post)
    {
        return $this->request('create', null, $post);
    }

    /**
     * 删除商品
     * @param string $productId
     * @return mixed
     */
    public function del($productId)
    {
        return $this->request('create', null, ['product_id' => $productId]);
    }

    /**
     * 修改商品
     * @param array $post
     * @return mixed
     */
    public function update(array $post)
    {
        return $this->request('update', null, $post);
    }

    /**
     * 查询商品
     * @param string $productId
     * @return mixed
     */
    public function get($productId)
    {
        return $this->request('create', null, ['product_id' => $productId]);
    }

    /**
     * 获取指定状态的所有商品
     * @param $status
     * @return mixed
     */
    public function getByStatus($status)
    {
        return $this->request('getbystatus', null, ['status' => $status]);
    }

    /**
     * 商品上下架
     * @param $productId
     * @param $status
     * @return mixed
     */
    public function modProductStatus($productId, $status)
    {
        return $this->request('modproductstatus', null, ['productId' => $productId, 'status' => $status]);
    }
}
