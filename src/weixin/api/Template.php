<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\api;

/**
 * Class Template
 */
class Template extends Api
{
    /**
     * 设置所属行业
     * @see http://mp.weixin.qq.com/wiki/17/304c1885ea66dbedf7dc170d84999a9d.html
     * @param array $post
     * @return mixed
     */
    public function apiSetIndustry(array $post)
    {
        return $this->request('template/api_set_industry', null, $post);
    }

    /**
     * 获得模板ID
     * @see http://mp.weixin.qq.com/wiki/17/304c1885ea66dbedf7dc170d84999a9d.html
     * @param $templateIdShort
     * @return mixed
     */
    public function apiAddTemplate($templateIdShort)
    {
        return $this->request('template/api_add_template', null, ['template_id_short' => $templateIdShort]);
    }
}
