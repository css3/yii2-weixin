<?php
namespace zonday\weixin;

use yii\base\Exception;

class InvalidResponseException extends Exception
{
    /**
     * @var array response headers.
     */
    public $responseHeaders = [];

    /**
     * @var string response body.
     */
    public $responseBody = '';

    /**
     * Constructor.
     * @param array $responseHeaders response headers
     * @param string $responseBody response body
     * @param string $message error message
     * @param integer $code error code
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($responseHeaders, $responseBody, $message = null, $code = 0, \Exception $previous = null)
    {
        $this->responseBody = $responseBody;
        $this->responseHeaders = $responseHeaders;
        parent::__construct($message, $code, $previous);
    }
}