<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\store;


use yii\base\Object;

/**
 * Class Store
 */
abstract class Store extends object
{
    public abstract function read($name);
    public abstract function write($name, array $data);
}