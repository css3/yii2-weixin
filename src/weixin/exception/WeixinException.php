<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\exception;

class WeixinException extends \yii\base\Exception
{
    public function getName()
    {
        return 'Weixin Exception';
    }
}