<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class Scan extends Event {
    public $EventKey;
    public $Ticket;

    public function getEvent()
    {
        return 'SCAN';
    }
}

