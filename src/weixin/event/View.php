<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class View extends Event {
    public $EventKey;

    public function getEvent()
    {
        return 'VIEW';
    }
}

