<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class Subscribe extends Event {
    public $EventKey;
    public $Ticket;

    public function getEvent()
    {
        return 'subscribe';
    }
}

