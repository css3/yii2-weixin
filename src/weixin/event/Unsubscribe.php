<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class Unsubscribe extends Event {
    public function getEvent()
    {
        return 'unsubscribe';
    }
}

