<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class Location extends Event {
    public $Latitude;
    public $Longitude;
    public $Precision;

    public function getEvent()
    {
        return 'LOCATION';
    }
}

