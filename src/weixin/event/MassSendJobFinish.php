<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class MassSendJobFinish extends Event
{
    public $MsgID;
    public $Status;
    public $TotalCount;
    public $FilterCount;
    public $SentCount;
    public $ErrorCount;

    public function getEvent()
    {
        return 'MASSSENDJOBFINISH';
    }
}

