<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class TemplateSendJobFinish extends Event
{
    public $MsgID;
    public $Status;

    public function getEvent()
    {
        return 'TEMPLATESENDJOBFINISH';
    }
}

