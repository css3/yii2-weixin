<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class ScanCodePush extends Event {
    public $EventKey;
    public $ScanCodeInfo;
    public $ScanType;
    public $ScanResult;

    public function getEvent()
    {
        return 'scancode_push';
    }
}

