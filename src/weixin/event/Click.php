<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

class Click extends Event {
    public $EventKey;

    public function getEvent()
    {
        return 'CLICK';
    }
}

