<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\event;

use yii\base\Object;

/**
 * Class Event
 * @property $MsgType
 */
abstract class Event extends object {
    public $ToUserName;

    public $FromUserName;

    public $CreateTime;

    public function getMsgType() {
        return 'event';
    }

    abstract public function getEvent();

    public function __set($name, $value) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        }
    }
}

