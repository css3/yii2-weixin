<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class Image extends Message
{
    public $PicUrl;
    public $MediaId;

    public function getMsgType()
    {
        return 'image';
    }

    public function __toString()
    {
        return <<<EOT
<xml>
<ToUserName><![CDATA[{$this->ToUserName}]]></ToUserName>
<FromUserName><![CDATA[{$this->FromUserName}]]></FromUserName>
<CreateTime>{$this->CreateTime}</CreateTime>
<MsgType><![CDATA[{$this->MsgType}]]></MsgType>
<Image>
<MediaId><![CDATA[{$this->MediaId}]]></MediaId>
</Image>
</xml>
EOT;
    }
}

