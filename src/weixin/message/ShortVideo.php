<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class ShortVideo extends Message
{
    public $MediaId;
    public $ThumbMediaId;

    public function getMsgType()
    {
        return 'shortvideo';
    }
}

