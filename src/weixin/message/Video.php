<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class Video extends Message
{
    public $MediaId;
    public $ThumbMediaId;

    public $Title;
    public $Description;

    public function getMsgType()
    {
        return 'video';
    }

    public function __toString()
    {
        return <<<EOT
<xml>
<ToUserName><![CDATA[{$this->ToUserName}]]></ToUserName>
<FromUserName><![CDATA[{$this->FromUserName}]]></FromUserName>
<CreateTime>{$this->CreateTime}</CreateTime>
<MsgType><![CDATA[{$this->MsgType}]]></MsgType>
<Video>
<MediaId><![CDATA[{$this->MediaId}]]></MediaId>
<Title><![CDATA[{$this->Title}]]></Title>
<Description><![CDATA[{$this->Description}]]></Description>
</Video>
</xml>
EOT;

    }
}

