<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

use yii\base\Object;

/**
 * Class Message
 * @property $MsgType
 */
abstract class Message extends Object {
    public $ToUserName;

    public $FromUserName;

    public $CreateTime;

    public $MsgId;

    abstract public function getMsgType();

    public function __toString()
    {
        return $this->getMsgType();
    }

    public function __set($name, $value) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        }
    }
}

