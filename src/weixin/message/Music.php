<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class Music extends Message
{
    public $MediaId;
    public $ThumbMediaId;

    public $Title;
    public $Description;
    public $MusicUrl;
    public $HQMusicUrl;

    public function getMsgType()
    {
        return 'music';
    }

    public function __toString()
    {
        return <<<EOT
<xml>
<ToUserName><![CDATA[{$this->ToUserName}]]></ToUserName>
<FromUserName><![CDATA[{$this->FromUserName}]]></FromUserName>
<CreateTime>{$this->CreateTime}</CreateTime>
<MsgType><![CDATA[{$this->MsgType}]]></MsgType>
<Music>
<Title><![CDATA[{$this->Title}]]></Title>
<Description><![CDATA[{$this->Description}]]></Description>
<MusicUrl><![CDATA[{$this->MusicUrl}]]></MusicUrl>
<HQMusicUrl><![CDATA[{$this->HQMusicUrl}]]></HQMusicUrl>
<ThumbMediaId><![CDATA[{$this->ThumbMediaId}]]></ThumbMediaId>
</Music>
</xml>
EOT;

    }
}

