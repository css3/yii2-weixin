<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class Link extends Message
{
    public $Title;
    public $Description;
    public $Url;

    public function getMsgType()
    {
        return 'link';
    }
}

