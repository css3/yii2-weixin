<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class Text extends Message
{
    public $Content;

    public function getMsgType()
    {
        return 'text';
    }

    public function __toString()
    {
        return <<<EOT
<xml>
<ToUserName><![CDATA[{$this->ToUserName}]]></ToUserName>
<FromUserName><![CDATA[{$this->FromUserName}]]></FromUserName>
<CreateTime>{$this->CreateTime}</CreateTime>
<MsgType><![CDATA[{$this->MsgType}]]></MsgType>
<Content><![CDATA[{$this->Content}]]></Content>
</xml>
EOT;
    }
}

