<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class News extends Message
{
    public $items = [];

    public function getMsgType()
    {
        return 'news';
    }

    public function __toString()
    {
        $count = count($this->items);
        $items = [];
        foreach ($this->items as $item) {
            $output = '';
            if (isset($item['title'])) {
                $output .= '<Title><![CDATA[' . $item['title'] . ']]></Title>';
            }
            if (isset($item['description'])) {
                $output .= '<Description><![CDATA[' . $item['description'] . ']]></Description>';
            }
            if (isset($item['picUrl'])) {
                $output .= '<PicUrl><![CDATA[' . $item['picUrl'] . ']]></PicUrl>';
            }
            if (isset($item['url'])) {
                $output .= '<Url><![CDATA[' . $item['url'] . ']]></Url>';
            }
            $items[] = '<item>' . $output . '</item>';
        }
        $items = implode("\n", $items);
        return <<<EOT
<xml>
<ToUserName><![CDATA[{$this->ToUserName}]]></ToUserName>
<FromUserName><![CDATA[{$this->FromUserName}]]></FromUserName>
<CreateTime>{$this->CreateTime}</CreateTime>
<MsgType><![CDATA[{$this->MsgType}]]></MsgType>
<ArticleCount>{$count}</ArticleCount>
<Articles>
{$items}
</Articles>
</xml>
EOT;
    }
}

