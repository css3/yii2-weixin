<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class Location extends Message
{
    public $Location_X;
    public $Location_Y;
    public $Scale;
    public $Label;

    public function getMsgType()
    {
        return 'location';
    }
}

