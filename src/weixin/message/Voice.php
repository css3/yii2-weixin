<?php
/**
 * @author css3 <css3@qq.com>
 */

namespace zonday\weixin\message;

class Voice extends Message
{
    public $MediaId;
    public $Format;
    public $Recognition;

    public function getMsgType()
    {
        return 'voice';
    }

    public function __toString()
    {
        return <<<EOT
<xml>
<ToUserName><![CDATA[{$this->ToUserName}]]></ToUserName>
<FromUserName><![CDATA[{$this->FromUserName}]]></FromUserName>
<CreateTime>{$this->CreateTime}</CreateTime>
<MsgType><![CDATA[{$this->MsgType}]]></MsgType>
<Voice>
<MediaId><![CDATA[{$this->MediaId}]]></MediaId>
</Voice>
</xml>
EOT;
    }
}

